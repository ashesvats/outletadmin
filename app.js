/**
 * @define module
 * @param {string} ngApp - parameter refers to the HTML element in which app will run
 * @param {Array} injector - loading modules through injector
 * */
var env = {};

// Import variables if present (from env.js)
if (window) {
    Object.assign(env, window.__env);
}

(function(angular){
    var app = angular.module('outletApp', ['ui.router', 'ngDialog', 'satellizer','daterangepicker']);

    //load enviorment variables into angular constants
    app.constant('__env',env);
    
    // load underscoreJS
    app.constant('_', window._);
    /** configure existing services */
    app.config(function ($stateProvider, $urlRouterProvider,$httpProvider, $authProvider,__env) {
        {
            $httpProvider.defaults.useXDomain = false;
            $httpProvider.defaults.withCredentials = false;
            //default configuration for statelizer
            $authProvider.loginUrl = __env.loginUrl + '/login';
            $authProvider.tokenName = 'jwt';
            $authProvider.tokenPrefix = 'hipcask';
            $authProvider.tokenHeader = 'X-HIP-KEY';
            // $authProvider.tokenType = 'Bearer';
            $authProvider.storageType = 'localStorage';

            // set LocalStorage service to avoid ambiguity
            // localStorageServiceProvider
            //     .setPrefix('hipcaskOutletApp')
            //     .setStorageType('localStorage')
            //     .setStorageCookie(45, '/', false)
            //     .setNotify(true, true)

        }
    });
    app.run(function($rootScope, $templateCache) {
        $templateCache.removeAll();
        console.log("template cleared");
    });
    function disableLogging($logProvider, __env){
        $logProvider.debugEnabled(__env.enableDebug);
    }

// Inject dependencies
    disableLogging.$inject = ['$logProvider', '__env'];

    app.config(disableLogging);
})(angular);