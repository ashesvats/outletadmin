/**
 * Rest Service
 */
(function(angular){
    angular.module('outletApp').service('restService', restService);

    function restService($http,$auth,$q,__env) {

        var _url=__env.apiUrl + '/outlet/';

        return {
            post: function (url, data) {
                return $q(function (resolve, reject) {
                    $http({
                        method: 'POST',
                        url: _url + url,
                        data: data,
                        headers: {
                            'Content-Type': 'application/json',
                            'X-HIP-KEY': $auth.getToken()
                        }
                    }).then(function (result) {
                        resolve(result);
                    }, function (error) {
                        reject(error);
                    });
                })
            },

            doGet: function (url) {
                return $q(function (resolve, reject) {
                    $http({
                        method: 'GET',
                        url: _url + url,
                        headers: {
                            'Content-Type': 'application/json',
                            'X-HIP-KEY': $auth.getToken()
                        }
                    }).then(function (result) {
                        resolve(result);
                    }, function (error) {
                        reject(error);
                    });
                })
            }
        }
    }
})(angular);