(function(angular){
    angular.module('outletApp').service('redemptionService', redemptionService);

    function redemptionService(restService,$q,_) {
        var api={}

        api.getRedemptions = function (data) {
            return $q(function (resolve, reject) {
                restService.post('redemptions/history', data).then(function (data) {
                    if (data.data.length > 0) {
                        resolve(data.data);
                    }
                    else resolve(false);
                }, function (error) {
                    console.log("error in fetching redemptions ", error);
                    reject(error);
                })
            });
        };
        return api
    }

})(angular);