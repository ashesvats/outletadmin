(function (angular) {
    angular.module('outletApp').service('passportService', passportService);

    function passportService(restService, $q) {
        var api = {};
        var current = []
        /*
         *  Get all check in passports
         *  @param {Object} JSON
         *  @paramExample {
         *                    lid:"67687dDWD8988979,
         *                     upid:"000" // required with garbage data
         *                    };
         * */
        api.getAll = function (data) {
            return $q(function (resolve, reject) {
                restService.post('checkin', data).then(function (data) {
                    if (data.data.length > 0) {
                        resolve(data.data);
                    }
                    else resolve(false);
                }, function (error) {
                    console.log("error in passport getAll Service ", error);
                    reject(error);
                })
            });
        };

        /*
         * Get al items related to passports
         * @param {Object} JSON
         * @paramExample {pid:'dh34343',lid:'34836hjfef'}
         * */
        api.getItems = function (data) {
            console.log("get items called");
            return $q(function (resolve, reject) {
                restService.post('checkin/items', data).then(function (response) {
                    if (response.data.length > 0) resolve(response.data);
                    else resolve([]);
                }, function (err) {
                    console.log("Error in retrieve passport items ", err);
                    reject(err);
                })
            })
        }

        api.redeem = function (data) {
            return $q(function (resolve, reject) {
                restService.post('redemptions', data).then(function (response) {
                    resolve(response.data);
                }, function (err) {
                    reject(err);
                })
            })
        }
        api.setCurrent = function (passport) {
            current = passport
        }
        api.getCurrent = function () {
            return current;
        }

        api.checkCoupon = function (data) {
            return $q(function (resolve, reject) {
                restService.post('redemptions/couponverify', data).then(function (response) {
                    resolve(response.data);
                }, function (err) {
                    reject(err);
                })
            })
        }

        api.redeemCoupon = function(data) {
             return $q(function (resolve, reject) {
                restService.post('redemptions/couponredeem', data).then(function (response) {
                    resolve(response.data);
                }, function (err) {
                    reject(err);
                })
            })
        }

        

        return api;
    }
})(angular);