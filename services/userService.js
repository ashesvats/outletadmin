(function(angular){
    angular.module('outletApp').service('userService', userService);

    function userService(restService,$q,_) {

        return {
            /*/
             * Find a user  by its id
             * @param {Number} id hipcask id
             * */
            getByID: function (id) {
                return $q(function(resolve,reject){
                    restService.doGet('user/' + id).then(function (result) {
                        resolve(result);
                    }, function (err) {
                        reject(err);
                    })
                })
            }
        }
    }

})(angular);