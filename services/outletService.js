(function(angular){
    angular.module('outletApp').service('outletService', outletService);

    function outletService($auth) {
        var outlet={};

        var payload=$auth.getPayload();
        outlet=payload.extra;
        /*
        * set outlet data
        * */
        this.put=function(data){
            outlet.id=data._id;
            outlet.name=data.name;
            outlet.lat=data.lat;
            outlet.lng=data.lng;
            outlet.contactPerson=data.contactPerson;
            outlet.contactPhone=data.contactMobile;
            outlet.locality=data.locality;
            console.log("seeting oulet data",outlet);
        }

        this.get=function(key){
            if(key) return outlet[key];
            else return outlet;
        }

        return this;
    }
})(angular);