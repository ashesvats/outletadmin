// app routes
angular.module('outletApp').config(config);

function config($stateProvider, $urlRouterProvider, $httpProvider) {
    /**
     * @default Login
     */
    $urlRouterProvider.otherwise('/passport');

    /** @define states */
    $stateProvider

        .state('app',{
            url:'/app',
            controller:'appCtrl',
            views: {
                'header': {
                    templateUrl: 'pages/template/header.html',
                    controller: 'headerCtrl'
                },
                'footer': {
                    templateUrl: 'pages/template/footer.html',
                    controller: function ($scope) { }
                }
            },
            abstract:true,
        })
        .state('login', {
            url: '/login',
             views: {
                'header': {
                    templateUrl: 'pages/template/blank_header.html',
                },
                'content': {
                    templateUrl: 'pages/account/login.html',
                    controller: 'loginCtrl'
                },
                'footer': {
                    templateUrl: 'pages/template/footer.html',
                    controller: function ($scope) { }
                }
            },
            onEnter: function () {
                console.log('login controller called');
            },
            resolve: {
                skipIfLoggedIn: skipIfLoggedIn
            }
        }).state('passport', {
        url: '/passport',
        resolve:{
            loginRequired:loginRequired
        },
        views: {
            'header': {
                templateUrl: 'pages/template/header.html',
                controller: 'headerCtrl'
            },
            'footer': {
                templateUrl: 'pages/template/footer.html',
                controller: function ($scope) { }
            },
            'content': {
                templateUrl: 'pages/passport/index.html',
                controller: 'passportCtrl'
            }
        },
        onEnter: function () {
            console.log('Passport Controller Called');
        }
        }).state('history', {
        url: '/history',
        resolve:{
            loginRequired:loginRequired
        },
        views: {
            'header': {
                templateUrl: 'pages/template/header.html',
                controller: 'headerCtrl'
            },
            'footer': {
                templateUrl: 'pages/template/footer.html',
                controller: function ($scope) { }
            },
            'content': {
                templateUrl: 'pages/history/index.html',
                controller: 'historyCtrl'
            }
        },
        onEnter: function () {
            console.log('History Controller Called');
        }
    }).state('items', {
        url: '/passport/items',
        resolve:{
            loginRequired:loginRequired
        },
        views: {
            'header': {
                templateUrl: 'pages/template/header.html',
                controller: 'headerCtrl'
            },
            'footer': {
                templateUrl: 'pages/template/footer.html',
                controller: function ($scope) { }
            },
            'content': {
                templateUrl: 'pages/passport/items.html',
                controller: 'itemCtrl',
            }
        },
        onEnter: function () {
            console.log('Items Controller Called');
        }
    });



    function skipIfLoggedIn($q, $auth, ngDialog) {
        var deferred = $q.defer();
        if ($auth.isAuthenticated()) {
            console.log('skipping auth true');
            ngDialog.open({
                template: "<h3 style='color: #4CBD50'>You are already logged in!!</h3>",
                className: 'ngdialog-theme-default',
                plain: true,
                overlay: true
            });
            deferred.reject();
        } else {
            console.log('auth failed');
            deferred.resolve();
        }
        return deferred.promise;
    }//end of function

    function loginRequired($q, $location, $auth) {
        var deferred = $q.defer();
        if ($auth.isAuthenticated()) {
            deferred.resolve();
        } else {
            $location.path('/login');
        }
        return deferred.promise;
    }//end of function

}