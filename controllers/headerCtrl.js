/**
 * header Controller
 */
(function(angular){
    angular.module('outletApp').controller('headerCtrl', headerCtrl);

function headerCtrl($scope, $state,$auth,outletService) {
    $scope.outletname=outletService.get('name');
    $scope.outletlocality=outletService.get('locality');
    $scope.outletcontactperson=outletService.get('contactName');

    $scope.logout=function(){
        $auth.logout();
        $state.go('login');
    }
}
})(angular);