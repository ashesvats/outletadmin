/**
 * Home Controller
 */
(function(angular){
    angular.module('outletApp').controller('historyCtrl', historyCtrl);

    function historyCtrl($scope, $q,$state,$auth,redemptionService) {
        $scope.redemptions=[];
        $scope.formData={};
        $scope.formData.perPage=1000;
        $scope.formData.date = {startDate: null, endDate: null};

        $scope.fetchHistory=function(data){
            console.log("fetch history called")
            data={"start":0,
                "perPage":1000
            }
            redemptionService.getRedemptions(data).then(function(result){
                $scope.redemptions=result
                console.log("history ",result)
            },function(err){
                console.log("error in fetching redemptions ",err)
            })
        }
        $scope.filterSubmit=function (data) {
            data.start=0
            var postData={
                start:0,
                date:{startDate:new Date(data.date.startDate._d).toISOString(),endDate:new Date(data.date.endDate._d).toISOString()},
                perPage:parseInt(data.perPage)
            }
            console.log("postData ",postData)
            redemptionService.getRedemptions(postData).then(function(result){

                $scope.redemptions=result
                console.log("history ",result)
            },function(err){
                console.log("error in fetching redemptions ",err)
            })
        }
    }
})(angular);