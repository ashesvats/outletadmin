/**
 * Checkin Controller
 */
(function (angular) {
    angular.module('outletApp').controller('itemCtrl', itemCtrl);

    function itemCtrl($scope,$state,$http, passportService, userService, outletService, $stateParams, ngDialog) {
        $scope.passports = [];
        $scope.items = [];
        $scope.quantity = [];
        $scope.redeemLeft = 0;
        $scope.selectedItems = [];
        $scope.currentPassport=[];
    
        /*
         *  Fetch all items related to selected passports
         *  @param {StateParam} pid - Passport ID
         * */
        $scope.fetchItems = function () {

            var passport = $scope.currentPassport = passportService.getCurrent();
            if(passport.length < 1) $state.go('passport');
            if (passport) {
                $scope.redeemLeft = passport.redeemsLeft;
                var postData = {
                    "lid": outletService.get("_id").toString(),
                    "uid": passport.userid,
                    "pid": passport.pid
                };
                passportService.getItems(postData).then(function (result) {
                    if (result.length > 0) {
                        $scope.items = result;
                        $scope.redeemLeft = passport.redeemsLeft;
                    }
                },function(err){
                    console.log("error in fetching items ",err);
                })
            }

        }
        $scope.getTotalItem=function(){
            var total=0;
            $scope.selectedItems.forEach(function(item){
                if(typeof item.quantity != 'undefined'){
                    total=total + item.quantity;
                }
            })
            return total;
        }

        $scope.showItems = function (passport) {
            passportService.setCurrent(passport)
            $state.go('items');
        }

        $scope.incr = function (index, item) {
            var value = parseInt($scope.quantity[index])
            $scope.quantity[index] = value + 1;
            updateItem(index, item, $scope.quantity[index])
        }

        $scope.decr = function (index,item) {
            var value = parseInt($scope.quantity[index])
            if (value >= 1) {
                $scope.quantity[index] = value - 1;
                updateItem(index, item, $scope.quantity[index])
            }
        }

        function updateItem(index, item, count) {
            if (typeof $scope.selectedItems[index] != 'undefined') {
                $scope.selectedItems[index].quantity = count;
            } else {
                $scope.selectedItems[index] = {name: item.name, iid: item.iid, quantity: count}
            }
        }

        $scope.confirmOrder=function(){
            ngDialog.openConfirm({
                template: 'templateId',
                // plain: true,
                className: 'ngdialog-theme-default',
                scope: $scope,
            }).then(function(accept){
                order();
            },function(reject){
                console.log("reject msg ",reject)
            });

        }

        function order()
        {
            var items=[];
            $scope.selectedItems.forEach(function(data){
                if(data.quantity!=0) items.push(data)
            });
            var passport = passportService.getCurrent();
            var outlet_name=outletService.get('name') + '(' + outletService.get('locality') +')';
            var postData={"upid":passport.userpassport,"items":items,"totalitems":$scope.getTotalItem(),"outlet_name":outlet_name};

            passportService.redeem(postData).then(function (result) {
                ngDialog.open({
                    template: '<p class="text-success">Successfully Redeemed</p>',
                    plain: true,
                    className: 'ngdialog-theme-plain',
                    scope: $scope,
                })
                $scope.selectedItems=[];

            for(var i=0;i<postData.items.length;i++) {

            setTimeout(function(x) {
                         return function() {
                               var evntData={
                                "Outlete": postData.outlet_name,
                                "Passport name":passport.passport.name,
                                "Item name": postData.items[x].name,
                                "Quantity": postData.items[x].quantity,
                                "Gender":passport.user.gender,
                                "Age Range":getAgeRange(passport.user.birthdate)
                    }
                    var obj ={
                               
                            "d": [
                                {
                                    "identity":passport.user.hipid,
                                    "type": "event",
                                    "evtName": "Redeemed",
                                    "evtData":evntData
                                    }
                        
                            ]
                    }

                       $http({
                        method: 'POST',
                        url:'https://api.clevertap.com/1/upload',
                        data: obj,
                        headers: {
                            'Content-Type': 'application/json',
                            'X-CleverTap-Account-Id':'WRK-R9Z-Z84Z',
                            'X-CleverTap-Passcode':'d0a238caf9b644068aa8c2bb760fef12'
                        }
                    }).then(function (result) {
                       
                    }, function (error) {
                    });
                    };
                     }(i), 2000*i);
                     }

        $state.go('passport');
            },function(err){
                ngDialog.open({
                    template: '<p class="bg-danger">Sorry,something went wrong !</p>',
                    plain: true,
                    className: 'ngdialog-theme-plain',
                    scope: $scope,
                })
                $scope.selectedItems=[]
                $state.go('passport');
            })
        }

        function getAgeRange(date) {
            var age = getAge(date);
            if(age>=21 && age<26) {
                return "21-25";
            } else if(age>25 && age<36) {
                return "25-35";
            } else if(age>35) {
                return "Above 35"
            } else {
                return "Unidentified";
            }
        }

        function getAge(dateString) {
                var today = new Date();
                var birthDate = new Date(dateString);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
                return age;
        }

    }
})(angular);