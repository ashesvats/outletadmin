/**
 * Checkin Controller
 */
(function (angular) {
    angular.module('outletApp').controller('passportCtrl', passportCtrl);

    function passportCtrl($scope,$auth, $interval, $state, passportService, userService, outletService, $stateParams, ngDialog) {
        $scope.passports = [];
        $scope.items = [];
        $scope.quantity = [];
        $scope.redeemLeft = 0;
        $scope.selectedItems = [];
        $scope.currentPassport=[];
        $scope.couponCode="";
        $scope.codeStatus="";
        $scope.couponId="";
        // watch for redeems left
        $scope.$watch(function (scope) {
                return scope.redeemLeft
            },
            function (n, o) {
            }
        );


        // fetch all currently avialble checked in users passports
        $scope.fetchPassports = function () {
            fetchPass();
        };
        if($auth.isAuthenticated()){
            function fetchPass() {
                var postData = {
                    lid: outletService.get("_id"),
                    upid: "000"
                };
                passportService.getAll(postData).then(function (data) {
                    if (data) {
                        console.log('passport',data);
                        $scope.passports = data;
                    }
                }, function (error) {
                    console.log("error in retrieving passports ");
                });
            }
        }

        if($auth.isAuthenticated()) {
            $interval(fetchPass, 10000);
        }
        /*
         *  Fetch all items related to selected passports
         *  @param {StateParam} pid - Passport ID
         * */
        $scope.fetchItems = function () {
            var passport = $scope.currentPassport = passportService.getCurrent();
            if (passport) {
                var postData = {
                    "lid": outletService.get("_id").toString(),
                    "pid": passport.pid
                };
                passportService.getItems(postData).then(function (result) {
                    if (result.length > 0) {
                        $scope.items = result;
                        $scope.redeemLeft = passport["passDetails"]["redeems_left"];
                    }
                })
            }
            else console.log("Parameter mismatch");
        }
        $scope.getTotalItem=function(){
            var total=0;
            $scope.selectedItems.forEach(function(item){
                if(typeof item.quantity != 'undefined'){
                    total=total + item.quantity;
                }
            })
            return total;
        }

        $scope.showItems = function (passport) {
            passportService.setCurrent(passport)
            $state.go('items');
        }

        $scope.applyPromoCode=function() {
                 

                 if($scope.couponCode.length<3) {
                   ngDialog.open({
                    template: '<p class="text-danger">Please Enter Valid Code</p>',
                    plain: true,
                    className: 'ngdialog-theme-plain',
                    scope: $scope,
                })
                return;
                 }

              var obj= {
                    "lid": outletService.get("_id").toString(),
                   
                    "code":$scope.couponCode
              }
              passportService.checkCoupon(obj).then(function(result) {
                
                ngDialog.openConfirm({
                template: 'couponverfy',
                // plain: true,
                className: 'ngdialog-theme-default',
                scope: $scope,
            }).then(function(accept){
                console.log("inside redeem")
               redeemOffer(result);
            },function(reject){
                $scope.couponCode="";
                console.log("reject msg ",reject)
            });


              },function(err){
                    $scope.couponCode="";
                  $scope.err= err.data.error;
                     ngDialog.open({
                    template: '<h1 style="color:red">Error</h1><p class="text-danger">{{err}}</p>',
                    plain: true,
                    className: 'ngdialog-theme-plain',
                    scope: $scope,
                })

              });
        }


        function redeemOffer(obj) {
             
            console.log("redeem Offer",obj);
            var data= {
                "sku": obj.sku,
                "uid": obj.uid,
                "couponId":obj.coupon_id,
                "campaign": obj.campaign,
                "code": obj.code
            }

   
            passportService.redeemCoupon(data).then(function(res){
                $scope.couponCode="";
                  ngDialog.open({
                    template: '<p class="text-success">Offer Successfully Redeemed</p>',
                    plain: true,
                    className: 'ngdialog-theme-plain',
                    scope: $scope,
                })

            },function(err){
                  $scope.couponCode="";
                  $scope.err= err.data.error;
                   ngDialog.open({
                    template: '<p class="text-danger">{{err}}</p>',
                    plain: true,
                    className: 'ngdialog-theme-plain',
                    scope: $scope,
                })

            })

        }


        
    }
})(angular);