/**
 * Login Controller
 */
(function(angular){
    angular.module('outletApp').controller('loginCtrl', loginCtrl);

function loginCtrl($scope, $state,$auth) {
    $scope.loginData={};

    $scope.login=function(){
        $auth.login($scope.loginData).then(function(data){
            $state.go('passport');
        },function(err){
            alert("Invalid username or password");
        });
    }
}
})(angular);